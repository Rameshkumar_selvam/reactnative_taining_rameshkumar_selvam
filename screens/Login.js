import React from 'react';

import { Component } from "react";
import { View, StyleSheet, Text, Alert } from "react-native";
import { Button, Divider, Badge, Avatar, Input } from 'react-native-elements';
import { NavigationContainer, CommonActions, Navigator } from '@react-navigation/native';

export default class Login extends Component {
    constructor(props) {
        super(props)
        this.state = {
            username: 'rameshkumar',
            password: '123'
        }
    }
    componentDidMount() {

    }


    render() {
        return (
            <>
                <View style={styles.container}>
                    <View style={styles.topViewStyle} />
                    <View style={styles.centerViewStyle} >
                        <View style={styles.centerViewStyle}>
                            <Input
                                label="User Name"
                                style={styles.usernameTextFieldStyle}
                                onChangeText={value => this.setState({ username: value })}
                                placeholder='please enter username'
                            >
                            </Input >
                            <Input style={styles.passwordTextFiledStyle}
                                label="Password"
                                secureTextEntry={true}>
                            </Input>
                            <Button
                                style={styles.usernameTextFieldStyle}
                                title="Login"
                                onPress={() => this.props.navigation.navigate('NewsListPage')}
                            />
                            <Text style={styles.usernameTextFieldStyle}>no need username password
                            </Text>
                        </View>
                    </View>
                    <View style={styles.bottumViewStyle} />
                </View>
            </>
        );
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 10,
        flexDirection: "column",
        backgroundColor: 'white',
        width: "100%",
        height: "100%",
        padding: 10,
        shadowRadius: 13,
        shadowColor: 'red'
    },
    topViewStyle: {
        flex: 1,
        backgroundColor: 'white'

    },
    bottumViewStyle: {
        flex: 1,
        backgroundColor: 'white'

    },
    centerViewStyle: {
        flex: 5,
        backgroundColor: 'white'
    },
    usernameTextFieldStyle: {
        padding: 10,
        backgroundColor: 'white'

    },

    passwordTextFiledStyle: {
        padding: 10

    },

    buttonText: {
        fontSize: 18,
        fontFamily: 'Gill Sans',
        textAlign: 'center',
        margin: 10,
        color: '#ffffff',
        backgroundColor: 'transparent',
    }

});