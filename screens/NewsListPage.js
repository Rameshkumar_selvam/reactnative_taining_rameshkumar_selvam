
import React, { Component } from "react";
import {
    SafeAreaView, View, Text, StyleSheet, FlatList, TouchableHighlight,
    Image, Button
} from "react-native";

import { businessButtonAction, entertainmentButtonAction, healthButtonAction, sportsButtonAction, technologyButtonAction } from './NewsListPageActions';
import { connect } from 'react-redux';
import {
    LearnMoreLinks,
    Colors,
    DebugInstructions,
    ReloadInstructions
} from 'react-native/Libraries/NewAppScreen';
import { ScrollView } from "react-native-gesture-handler";
import { CustomPicker } from 'react-native-custom-picker'
import RNPickerSelect from 'react-native-picker-select';

class NewsListPage extends Component {

    constructor(props) {
        super(props)
        this.state = {
            articles: [],
            selectedCategoty: ''
        }
    }
    componentDidMount() {
        console.log(this.props)
        this.props.businessButtonAction()
    }
    _renderItem(item, index) {
        return (
            <View>
                <TouchableHighlight style={styles.sectionContainer}
                    onPress={() => this.props.navigation.navigate('NewsWebView', { url: item.url })}
                >
                    <View style={{ padding: 10, backgroundColor: '#FFFFFF', borderRadius: 10 }}>

                        <Text numberOfLines={0} style={styles.sectionTitle}>{item.title}</Text>

                        {(item.urlToImage) ?
                            <Image style={{ height: 250, width: null, borderRadius: 0, backgroundColor: '#808080' }} resizeMode="stretch" source={{ uri: item.urlToImage }} />
                            : <Text>{item.author}</Text>
                        }
                        <Text style={styles.sectionDescription}>{item.description}</Text>
                        <Text style={styles.sectionDescription}>{item.content}</Text>
                        <Text style={styles.footer}>{item.author}</Text>

                    </View>
                </TouchableHighlight>
            </View>);
    }
    _renderNewsCategoryView() {
        return (
            <ScrollView horizontal={true}>
                <View style={{ height: 40, flexDirection: 'row', padding: 2 }}>
                    <Button
                        backgroundColor={Colors.black}
                        style={styles.categoryButtonStyle}
                        title={'Business'}
                        onPress={() => { this.props.businessButtonAction() }}
                        status='error'
                    />
                    <Button
                        style={styles.categoryButtonStyle}
                        title="Entertainment"
                        onPress={() => this.props.entertainmentButtonAction()}
                    />
                    <Button
                        style={styles.categoryButtonStyle}
                        title="Health"
                        onPress={() => this.props.healthButtonAction()}
                        status='error'
                    />
                    <Button
                        style={styles.categoryButtonStyle}

                        title="Sports"
                        onPress={() => this.props.sportsButtonAction()}
                    />
                    <Button
                        style={styles.categoryButtonStyle}

                        title="Tech "
                        onPress={() => this.props.technologyButtonAction()}

                    />
                </View>
            </ScrollView>
        );
    }
    _rendreSelectionView() {
        return (
            < View style={styles.selectionContainerView}>
                <View>
                    <Text>Category:</Text>
                    <RNPickerSelect
                        style={styles.selectionView}
                        onValueChange={(value) => this.categorySelectionAction(value)}
                        title={'test'}
                        items={[
                            { label: 'Business', value: 'business', color: 'red' },
                            { label: 'Entertainment', value: 'entertainment' },
                            { label: 'Health', value: 'health' },
                            { label: 'Sports', value: 'sports' },
                        ]}
                    />
                </ View>
            </View >
        );
    }
    render() {
        return (
            <>
                <SafeAreaView />
                <View style={styles.bodyStyle}>
                    <View >
                        {this._renderNewsCategoryView()}
                        {this._rendreSelectionView()}
                    </View>
                    <FlatList data={this.props.articles}
                        renderItem={({ index, item }) =>
                            this._renderItem(item)
                        }
                    />
                </View>
                <SafeAreaView />
                <SafeAreaView />
            </>
        );
    }

    categorySelectionAction(category) {
        switch (category) {
            case 'business':
                this.props.businessButtonAction();
                break;
            case 'entertainment':
                this.props.entertainmentButtonAction();
                break;
            case 'health':
                this.props.healthButtonAction();
                break;
            case 'sports':
                this.props.sportsButtonAction();
                break;
            case 'technology':
                this.props.technologyButtonAction();
                break;
        }
    }
}


const mapDispatchToProps = {
    businessButtonAction,
    entertainmentButtonAction,
    healthButtonAction,
    sportsButtonAction,
    technologyButtonAction
}

function mapStateToProps(state) {
    return {
        articles: state.articles,
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(NewsListPage)

const styles = StyleSheet.create({
    selectionContainerView: {
        flexDirection: 'row',
        padding: 10,
        marginHorizontal: 0,
        backgroundColor: 'white',
        height: 60,
        justifyContent: 'space-between',
    },
    selectionView: {
        padding: 10,
        backgroundColor: 'red',
        alignContent: 'center'

    },
    scrollView: {
        backgroundColor: Colors.lighter,
    },
    engine: {
        position: 'absolute',
        fontWeight: '600',
        right: 10,
        top: 10
    },
    body: {
        backgroundColor: Colors.white,
    },
    sectionContainer: {
        marginTop: 20,
        paddingHorizontal: 5,
        shadowColor: '#808080',
        shadowRadius: 5, shadowOpacity: 4
    },
    sectionTitle: {
        marginTop: 8,
        marginBottom: 8,
        fontSize: 20,
        fontWeight: '600',
        color: Colors.black,
    },
    sectionDescription: {
        marginTop: 8,
        fontSize: 18,
        fontWeight: '400',
        color: Colors.dark,
    },
    highlight: {
        fontWeight: '700',
    },
    seperatore:
    {
        height: 2,
        backgroundColor: Colors.black,
        marginTop: 18,

    },
    footer: {
        color: Colors.dark,
        fontSize: 12,
        fontWeight: '600',
        padding: 4,
        paddingRight: 12,
        textAlign: 'right',
    },
    bodyStyle: {
        padding: 2,
        paddingBottom: 10,


    },
    categoryButtonStyle:
    {
        flex: 1,
        padding: 4,

    }
});