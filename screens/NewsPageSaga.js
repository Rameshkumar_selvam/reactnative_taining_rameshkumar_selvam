import { takeEvery, takeLatest, put, call } from 'redux-saga/effects'
import {
    businessButtonAction,
    getActionType,
    storeEntertainmentDataAction,
    entertainmentButtonAction,
    storebusinessDataAction,
    healthButtonAction,
    sportsButtonAction,
    storeHealthDataAction,
    storeSportsDataAction,
    technologyButtonAction,
    storeTechnologyDataAction,
} from './NewsListPageActions'
import { all } from 'redux-saga/effects';
import { YellowBox } from 'react-native';
import { fetchArticals } from './Api'

function* bussinessAsync() {
    const res = yield call(fetchArticals, 'business')
    const { articles } = res;
    yield put(storebusinessDataAction({ articles: articles }))

}
function* entertainmentAsync() {

    const res = yield call(fetchArticals, 'entertainment')
    const { articles } = res;
    yield put(storeEntertainmentDataAction({ articles: articles }));

}

function* healthAsync() {
    const res = yield call(fetchArticals, 'health')
    const { articles } = res;
    yield put(storeHealthDataAction({ articles: articles }))
}

function* sportsAsync() {
    const res = yield call(fetchArticals, 'Sports')
    const { articles } = res
    yield put(storeSportsDataAction({ articles: articles }));
}
function* technologyAsync() {
    const res = yield call(fetchArticals, 'technology')
    const { articles } = res
    yield put(storeTechnologyDataAction({ articles: articles }));
}

export const newsPageSaga = [

    takeLatest(getActionType(businessButtonAction), bussinessAsync),
    takeLatest(getActionType(entertainmentButtonAction), entertainmentAsync),
    takeLatest(getActionType(healthButtonAction), healthAsync),
    takeLatest(getActionType(sportsButtonAction), sportsAsync),
    takeLatest(getActionType(technologyButtonAction), technologyAsync)
];

export function* sagas() {
    yield all([...newsPageSaga])
}

