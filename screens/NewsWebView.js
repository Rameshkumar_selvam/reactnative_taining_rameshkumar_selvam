
import React, { Component } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { WebView } from 'react-native-webview';
 
// ...
export default class NewsWebView extends Component {

  componentDidMount()
  {
    console.log(this.props)
  }
  render() {
    return <WebView source={{ uri: this.props.route.params.url}} />;
  }
}