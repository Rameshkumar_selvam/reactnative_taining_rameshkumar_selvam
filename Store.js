import { createStore, applyMiddleware } from 'redux'
import createSagaMiddleware from 'redux-saga'
import { sagas } from './screens/NewsPageSaga'
import { combineReducers } from "redux";

import { newsListreducer } from './screens/NewsListPageReducer'

const sagaMiddleware = createSagaMiddleware()

export const store = createStore(newsListreducer, applyMiddleware(sagaMiddleware))

store.subscribe(() => {
    if (__DEV__) {
      console.log("Store Changed ", store.getState());
    }
  });
  
export default function configureStore() {
    sagaMiddleware.run(sagas);
    return store;
}

