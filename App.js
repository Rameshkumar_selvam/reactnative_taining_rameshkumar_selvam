import 'react-native-gesture-handler';
import React from 'react';
import { NavigationContainer,CommonActions, Navigator } from '@react-navigation/native';
import {  createStackNavigator} from '@react-navigation/stack';
import Login from './screens/Login';
import NewsListPage from './screens/NewsListPage';
import NewsWebView from './screens/NewsWebView';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';

import configureStore from './Store'
import { Provider } from 'react-redux'
import createSagaMiddleware from 'redux-saga'
import {createDrawerNavigator} from '@react-navigation/drawer'

const Stack = createStackNavigator();
const Drawer = createDrawerNavigator()
const Tab = createMaterialTopTabNavigator();

const myStore = configureStore()

function MyDrawer()
{
  return(
  <Drawer.Navigator initialRouteName="NewsListPage">
         <Drawer.Screen name="MyStack"  component={NewsListPage} />
    </Drawer.Navigator>
    );
}

function MyStack() {
  return (
    <Provider store = {myStore}>
	  <Stack.Navigator   initialRouteName="Login">
         <Stack.Screen name="Login"  component={Login}   />
         <Stack.Screen name="NewsListPage"  component={NewsListPage} canGoBack = {false}  />
         <Stack.Screen name="NewsWebView"  component={NewsWebView}   initialParams={{ url: 'https://reactnavigation.org/docs/params' }}
  />
         
	   </Stack.Navigator>
     </Provider>
  );
}

export default function App() {
  return (
    <NavigationContainer>
      <MyStack/>
    </NavigationContainer>
  );
}




